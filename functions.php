<?php
function divi__child_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'divi__child_theme_enqueue_styles' );
 
 
//you can add custom functions below this line:
add_filter('cf7_2_post_filter-project-editor','filter_project_editor',10,3);

function filter_project_editor($value, $post_id, $form_data){

  //$value is the post field value to return, by default it is empty. If you are filtering a taxonomy you can return either slug/id/array.  in case of ids make sure to cast them integers.(see https://codex.wordpress.org/Function_Reference/wp_set_object_terms for more information.)
  //$post_id is the ID of the post to which the form values are being mapped to
  // $form_data is the submitted form data as an array of field-name=>value pairs
  

function makeimageattach($filename_field){

  // Add Featured Image to Post
  $upload_dir       = wp_upload_dir(); // Set upload folder
  $filename         = basename( $filename_field ); // Create image file name

  // Check folder permission and define file location
  if( wp_mkdir_p( $upload_dir['path'] ) ) {
      $file = $upload_dir['path'] . '/' . $filename;
  } else {
      $file = $upload_dir['basedir'] . '/' . $filename;
  }


  // Check image file type
  $wp_filetype = wp_check_filetype( $filename, null );

  // Set attachment data
  $attachment = array(
      'post_mime_type' => $wp_filetype['type'],
      'post_title'     => sanitize_file_name( $filename ),
      'post_content'   => '',
      'post_status'    => 'inherit'
  );

  // Create the attachment
  $attach_id = wp_insert_attachment( $attachment, $file );

  // Include image.php
  require_once(ABSPATH . 'wp-admin/includes/image.php');

  // Define attachment metadata
  $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

  // Assign metadata to attachment
  wp_update_attachment_metadata( $attach_id, $attach_data );

  return $attach_id;

}
/*Start of LP code */
/*End of LP code */

if(isset($form_data)){
/*Start of LP code */	
/*End of LP code */

if(!empty($form_data['kariera-dalsifotografie1'])){
  $image1_id = makeimageattach($form_data['kariera-dalsifotografie1']);
}

if(!empty($form_data['kariera-dalsifotografie2'])){
  $image2_id = makeimageattach($form_data['kariera-dalsifotografie2']);
}

if(!empty($form_data['kariera-dalsifotografie3'])){
  $image3_id = makeimageattach($form_data['kariera-dalsifotografie3']);
}

if(!empty($form_data['kariera-dalsifotografie4'])){
  $image4_id = makeimageattach($form_data['kariera-dalsifotografie4']);
}

}

  $data = '[et_pb_section fb_built="1" _builder_version="3.22.7"][et_pb_row _builder_version="3.22.7"][et_pb_column type="1_2" _builder_version="3.22.7"][et_pb_post_title title="off" meta="off" _builder_version="3.22.7"][/et_pb_post_title]';

  if(!empty($image1_id) || !empty($image2_id) || !empty($image3_id) || !empty($image4_id)){
    $data .= '[et_pb_gallery gallery_ids="';

    if(!empty($image1_id)){
      $data .= $image1_id . ',';
    }

    if(!empty($image2_id)){
      $data .= $image2_id . ',';
    }

    if(!empty($image3_id)){
      $data .= $image3_id . ',';
    }

    if(!empty($image4_id)){
      $data .= $image4_id . ',';
    }

    $data .= '" show_title_and_caption="off" _builder_version="3.22.7"][/et_pb_gallery]';
  }


  $data .= '[/et_pb_column][et_pb_column type="1_2" _builder_version="3.22.7"][et_pb_text _builder_version="3.22.7" _dynamic_attributes="content" text_font="|600|||||||" text_font_size="30px" header_font="Poppins|600|||||||" header_text_align="center" header_font_size="56px" text_orientation="center" custom_margin="50px||"]@ET-DC@eyJkeW5hbWljIjp0cnVlLCJjb250ZW50IjoicG9zdF90aXRsZSIsInNldHRpbmdzIjp7ImJlZm9yZSI6IiIsImFmdGVyIjoiIn19@[/et_pb_text][et_pb_divider color="#f7d584" divider_weight="3px" disabled_on="on|on|off" _builder_version="3.2" max_width="80px" module_alignment="center" height="10px" custom_margin="||20px|" animation_style="slide" animation_direction="bottom" saved_tabs="all" locked="off"][/et_pb_divider]';


  $data .= '[et_pb_text _builder_version="3.22.7" text_font="Poppins||||||||" text_text_color="rgba(0,0,0,0.6)" text_font_size="16px" text_line_height="1.9em" header_font="||||||||" header_2_font="||||||||" text_orientation="center" module_alignment="center" custom_margin="50px||50px|" locked="off"]<p>';

if(!empty($form_data['kariera-vek'])){
  $data .= '<b>Věk:</b><br /> '.$form_data['kariera-vek'].' let<br />';
}
if(!empty($form_data['kariera-konfekcnivelikost'])){
  $data .= '<b>Konfekční velikost:</b><br /> '.$form_data['kariera-konfekcnivelikost'].'<br />';
}
if(!empty($form_data['kariera-velikostobuvi'])){
  $data .= '<b>Velikost obuvi:</b><br /> '.$form_data['kariera-velikostobuvi'].'<br />';
}
if(!empty($form_data['kariera-barvavlasu'])){
  $data .= '<b>Barva vlasů:</b><br /> '.$form_data['kariera-barvavlasu'].'<br />';
}
if(!empty($form_data['kariera-miry'])){
  $data .= '<b>Míry:</b><br /> '.$form_data['kariera-miry'].'<br />';
}
if(!empty($form_data['kariera-vyska'])){
  $data .= '<b>Výška:</b><br /> '.$form_data['kariera-vyska'].'<br />';
}
if(!empty($form_data['kariera-jazykauroven'])){
  $data .= '<b>Jazyky a úroveň:</b><br /> '.$form_data['kariera-jazykauroven'].'<br />';
}
	if(!empty($form_data['kariera-mesto'])){
  $data .= '<b>Město:</b><br /> '.$form_data['kariera-mesto'].'';
}

  $data .= '</p>[/et_pb_text]';


  $data .= '[et_pb_button button_url="/kontakt/" button_text="Kontaktujte nás" button_alignment="center" _builder_version="3.22.7" custom_button="on" button_text_size="14px" button_text_color="#ffffff" button_bg_color="#d3aa55" button_border_width="0px" button_border_radius="0px" button_letter_spacing="2px" button_font="Poppins|600||on|||||" button_use_icon="off" box_shadow_style="preset1" box_shadow_vertical="10px" box_shadow_blur="30px" box_shadow_color="#f3d178" custom_margin="50px|||" custom_padding="12px|28px|12px|28px" button_letter_spacing_hover="2px" button_text_size__hover_enabled="off" button_one_text_size__hover_enabled="off" button_two_text_size__hover_enabled="off" button_text_color__hover_enabled="off" button_one_text_color__hover_enabled="off" button_two_text_color__hover_enabled="off" button_border_width__hover_enabled="off" button_one_border_width__hover_enabled="off" button_two_border_width__hover_enabled="off" button_border_color__hover_enabled="off" button_one_border_color__hover_enabled="off" button_two_border_color__hover_enabled="off" button_border_radius__hover_enabled="off" button_one_border_radius__hover_enabled="off" button_two_border_radius__hover_enabled="off" button_letter_spacing__hover_enabled="on" button_letter_spacing__hover="2px" button_one_letter_spacing__hover_enabled="off" button_two_letter_spacing__hover_enabled="off" button_bg_color__hover_enabled="off" button_one_bg_color__hover_enabled="off" button_two_bg_color__hover_enabled="off"][/et_pb_button][/et_pb_column][/et_pb_row][/et_pb_section][et_pb_section fb_built="1" disabled_on="off|off|off" admin_label="Footer" _builder_version="3.22.7" use_background_color_gradient="on" background_color_gradient_start="#d3aa55" background_color_gradient_end="#f3d178" background_color_gradient_direction="360deg" max_width="80%" max_width_last_edited="on|phone" module_alignment="center" custom_padding="110px|0px|110px|0px" animation_style="slide" animation_direction="bottom" animation_intensity_slide="14%" animation_starting_opacity="100%" global_module="201" saved_tabs="all" locked="off"][et_pb_row _builder_version="3.22.3" background_color_gradient_direction="38deg" module_alignment="center" locked="off"][et_pb_column type="1_2" _builder_version="3.0.47"][et_pb_text _builder_version="3.22.7" header_font="||||||||" header_2_font="||||||||" header_2_text_color="#ffffff" header_3_font="Roboto||||||||" header_3_text_align="center" header_3_text_color="#ffffff" header_3_font_size="35px"]<h3>Máte zájem o naše služby?</h3>[/et_pb_text][et_pb_button button_text="Zaslat nezávaznou poptávku" button_alignment="center" _builder_version="3.22.7" custom_button="on" button_text_color="#ffffff" button_font="||||||||"][/et_pb_button][/et_pb_column][et_pb_column type="1_2" _builder_version="3.0.47"][et_pb_text _builder_version="3.22.7" header_font="||||||||" header_2_font="||||||||" header_2_text_color="#ffffff" header_3_font="Roboto||||||||" header_3_text_align="center" header_3_text_color="#ffffff" header_3_font_size="35px"]<h3>Kontakt</h3>[/et_pb_text][et_pb_text _builder_version="3.22.7" text_font="Poppins||||||||" text_text_color="rgba(0,0,0,0.5)" text_font_size="16px" text_line_height="1.9em" header_font="||||||||" header_4_font="|700||on|||||" header_4_font_size="16px" header_4_letter_spacing="1px" header_4_line_height="1.5em" text_orientation="center" background_layout="dark" custom_padding="|||||" locked="off"]<p><em>+420 603 200 553</em><br /> <em>hana@hanatomaskova.cz</em></p>[/et_pb_text][/et_pb_column][/et_pb_row][/et_pb_section]';
  
  return $data;

  
}

add_filter('et_theme_image_sizes', 'return_my_image_sizes');

function return_my_image_sizes( $sizes )
{
	$image_sizes = array(
		'400x250'   => 'et-pb-post-main-image',
		'1080x675'  => 'et-pb-post-main-image-fullwidth',
		'400x500'   => 'et-pb-portfolio-image',
		'510x382'   => 'et-pb-portfolio-module-image',
		'1080x9999' => 'et-pb-portfolio-image-single',
	);
	return $image_sizes;
}

// Begin custom image size for Portfolio Module
add_filter( 'et_pb_portfolio_image_height', 'portfolio_size_h' );
add_filter( 'et_pb_portfolio_image_width', 'portfolio_size_w' );

function portfolio_size_h($height) {
	return '400';
}

function portfolio_size_w($width) {
	return '400';
}

add_image_size( 'custom-portfolio-size', 400, 400, array( 'center', 'top' ));
// End custom image size for Portfolio Module
